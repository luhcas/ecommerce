<?php

$soma_altura=10;
$soma_comprimento=10;
$soma_largura=10;
$soma_peso=1;
$cep_cliente='02471210';

$parametros = array();
		
		// Código e senha da empresa, se você tiver contrato com os correios, se não tiver deixe vazio.
		$parametros['nCdEmpresa'] = '';
		$parametros['sDsSenha'] = '';
		
		// CEP de origem e destino. Esse parametro precisa ser numérico, sem "-" (hífen) espaços ou algo diferente de um número.
		$parametros['sCepOrigem'] = '02012021';
		$parametros['sCepDestino'] = $cep_cliente;
		
		// O peso do produto deverá ser enviado em quilogramas, leve em consideração que isso deverá incluir o peso da embalagem.
		$parametros['nVlPeso'] = $soma_peso;
		
		// O formato tem apenas duas opções: 1 para caixa / pacote e 2 para rolo/prisma.
		$parametros['nCdFormato'] = '1';
		
		// O comprimento, altura, largura e diametro deverá ser informado em centímetros e somente números
		$parametros['nVlComprimento'] = $soma_comprimento;
		$parametros['nVlAltura'] = $soma_altura;
		$parametros['nVlLargura'] = $soma_largura;
		$parametros['nVlDiametro'] = '0';
		
		// Aqui você informa se quer que a encomenda deva ser entregue somente para uma determinada pessoa após confirmação por RG. Use "s" e "n".
		$parametros['sCdMaoPropria'] = '0';
		
		// O valor declarado serve para o caso de sua encomenda extraviar, então você poderá recuperar o valor dela. Vale lembrar que o valor da encomenda interfere no valor do frete. Se não quiser declarar pode passar 0 (zero).
		$parametros['nVlValorDeclarado'] = '0';
		
		// Se você quer ser avisado sobre a entrega da encomenda. Para não avisar use "n", para avisar use "s".
		$parametros['sCdAvisoRecebimento'] = 'n';
		
		// Formato no qual a consulta será retornada, podendo ser: Popup – mostra uma janela pop-up | URL – envia os dados via post para a URL informada | XML – Retorna a resposta em XML
		$parametros['StrRetorno'] = 'xml';
		
		// Código do Serviço, pode ser apenas um ou mais. Para mais de um apenas separe por virgula.
		$parametros['nCdServico'] = '40010,41106';
		
		$parametros = http_build_query($parametros);
	$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
	$curl = curl_init($url.'?'.$parametros);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$dados = curl_exec($curl);
	$dados = simplexml_load_string($dados);
	
	foreach($dados->cServico as $linhas) {
		if($linhas->Erro == 0) {
			echo $linhas->Codigo.'</br>';
			echo $linhas->Valor .'</br>';
			echo $linhas->PrazoEntrega.' Dias </br>';
		}else {
			echo $linhas->MsgErro;
		}
		echo '<hr>';
		}
?>