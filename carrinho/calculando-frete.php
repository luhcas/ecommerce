<?php
	if($soma_peso<0.5){
		if($soma_peso<=0.020){
			$frete = '6.00';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.020 && $soma_peso<=0.050){
			$frete = '6.65';
			echo'R$' . $frete;
		}
		elseif($soma_peso>0.050 && $soma_peso<=0.100){
			$frete = '7.55';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.100 && $soma_peso<=0.150){
			$frete = '8.30';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.150 && $soma_peso<=0.200){
			$frete = '9.00';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.200 && $soma_peso<=0.250){
			$frete = '9.70';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.250 && $soma_peso<=0.300){
			$frete = '10.50';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.300 && $soma_peso<=0.350){
			$frete = '11.25';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.350 && $soma_peso<=0.400){
			$frete = '11.95';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.400 && $soma_peso<=0.450){
			$frete = '12.65';
			echo'R$ ' . $frete;
		}
		elseif($soma_peso>0.450 && $soma_peso<=0.500){
			$frete = '13.40';
			echo'R$ ' . $frete;
		}
	}
	else{
		$parametros = array();
		
		// C�digo e senha da empresa, se voc� tiver contrato com os correios, se n�o tiver deixe vazio.
		$parametros['nCdEmpresa'] = '';
		$parametros['sDsSenha'] = '';
		
		// CEP de origem e destino. Esse parametro precisa ser num�rico, sem "-" (h�fen) espa�os ou algo diferente de um n�mero.
		$parametros['sCepOrigem'] = '02012021';
		$parametros['sCepDestino'] = $cep_cliente;
		
		// O peso do produto dever� ser enviado em quilogramas, leve em considera��o que isso dever� incluir o peso da embalagem.
		$parametros['nVlPeso'] = $soma_peso;
		
		// O formato tem apenas duas op��es: 1 para caixa / pacote e 2 para rolo/prisma.
		$parametros['nCdFormato'] = '1';
		
		// O comprimento, altura, largura e diametro dever� ser informado em cent�metros e somente n�meros
		$parametros['nVlComprimento'] = $soma_comprimento;
		$parametros['nVlAltura'] = $soma_altura;
		$parametros['nVlLargura'] = $soma_largura;
		$parametros['nVlDiametro'] = '0';
		
		// Aqui voc� informa se quer que a encomenda deva ser entregue somente para uma determinada pessoa ap�s confirma��o por RG. Use "s" e "n".
		$parametros['sCdMaoPropria'] = '0';
		
		// O valor declarado serve para o caso de sua encomenda extraviar, ent�o voc� poder� recuperar o valor dela. Vale lembrar que o valor da encomenda interfere no valor do frete. Se n�o quiser declarar pode passar 0 (zero).
		$parametros['nVlValorDeclarado'] = '0';
		
		// Se voc� quer ser avisado sobre a entrega da encomenda. Para n�o avisar use "n", para avisar use "s".
		$parametros['sCdAvisoRecebimento'] = 'n';
		
		// Formato no qual a consulta ser� retornada, podendo ser: Popup � mostra uma janela pop-up | URL � envia os dados via post para a URL informada | XML � Retorna a resposta em XML
		$parametros['StrRetorno'] = 'xml';
		
		// C�digo do Servi�o, pode ser apenas um ou mais. Para mais de um apenas separe por virgula.
		$parametros['nCdServico'] = '41106';
		
		$parametros = http_build_query($parametros);
		$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
		$curl = curl_init($url.'?'.$parametros);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$dados = curl_exec($curl);
		$dados = simplexml_load_string($dados);
		
		foreach($dados->cServico as $linhas) {
			if($linhas->Erro == 0) {
				echo $linhas->Valor.'</br>';
				$frete = $linhas->Valor ;
			}else {
				echo $linhas->MsgErro;
			}
		}
	}
?>