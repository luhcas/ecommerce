<?php
session_start();
if(isset($_SESSION['slogin']) && isset($_SESSION['ssenha'])){
?>

<?php
$page_title = 'Home Page';
include('../include/header.html');
?>

<?php
include('../include/aside.html');
?>
<article>
	<font face="Arial" size="4" color="black"align="center"> 
		<br />
		<h2>NOSSA MISSÃO</h2>

		<br />
		A INDÚSTRIA DE MÓVEIS ESCTECX, COM SEU LOGO "TUDO PARA MÓVEIS" 
		TEM COMO PRINCIPAL OBJETIVO OFERECER PRODUTOS DIFERENCIADOS COM SOLUÇÕES EFICAZES E CRIATIVAS PARA 
		SEUS CLIENTES, COM ALTA QUALIDADE E DESIGN INOVADOR.
		<br /><br /><br /> 
		SER RECONHECIDA COMO A EMPRESA QUE OFERECE AS MELHORES SOLUÇÕES EM 
		PRODUTOS RESIDENCIAIS QUE AUMENTEM O CONFORTO E BEM ESTAR DAS PESSOAS, FORNECENDO 
		PRODUTOS INOVADORES, VERSÁTEIS, QUE POSSAM ATENDER A NECESSIDADE DO PUBLICO MELHORANDO A
		QUALIDADE DE VIDA E PROSPERANDO COMO EMPRESA COM SUSTENTABILIDADE AMBIENTAL E SOCIAL.	
	</font>
</article>
<?php
include('../include/footer.html');
?>

<?php
}
else{
	header("location:../index.php");
}
?>