﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>

<?php
include("../conexao/conexao.php");
$sel = "select * from pedido";
$execbanco=mysqli_query($conexao,$sel);
?>
<table width='49%' border='1' align='left'>
	<tr align='center'>
		<td width='45%'>Pedidos em Aberto (5 Mais Antigos)</td> 
		<td width='5%' colspan = '2'><a href='pedidos_em_aberto.php'>ver todos</a></td>	
	</tr>
	<tr align='center'>
		<td>Nº</td>
		<td>Detalhe</td>
		<td>Separar</td>
	</tr>
	<?php
		$verifica = "Aberto";
		include('pedido_include.html');
		include('aberto.php');
	?>
</table>

<table border='1' width='49%'  align='right'>
	<tr align='center'>
		<td width='45%' colspan = '5'>Pedidos Separados (5 Mais Antigos)</td>
		<td width='5%' colspan = '1'><a href='pedidos_separados.php'>ver todos</a></td>
	<tr>
	<tr align='center'>
		<td>Nº</td>
		<td>Detalhe</td>
		<td>Endereço</td>
		<td>Rastreio</td>
		<td>Data Envio</td>
		<td>Enviar</td>
	</tr>
	<?php
	$execbanco=mysqli_query($conexao,$sel);
	$verifica = "Separado";
	include('pedido_include.html');
	include('separado.php');
	?>
</table>
		
			<br><br><br><br><br><br><br><br><br><br><br>
			<hr>

<table border='1' align='left' width='49%'>
	<tr align='center'>
		<td width='45%' colspan = '5'>Pedidos em Tránsito (5 Mais Antigos)</td>
		<td width='5%' colspan = '1'><a href='pedidos_em_transito.php'>ver todos</a></td>
	</tr>
	<tr align='center'>
		<td>Nº</td>
		<td>Detalhe</td>
		<td>Data Envio</td>
		<td>Rastreio</td>
		<td>Data Entrega</td>
		<td>Entregar</td>
	</tr>
	<?php
	$execbanco=mysqli_query($conexao,$sel);
	$verifica = "Transito";
	include('pedido_include.html');
	include('transito.php');
	?>
</table>

<table border='1' align='right' width='49%'>
	<tr align='center'>
		<td width='40%' colspan = '5'>Pedidos Encerrados (5 ultimos)</td>
		<td width='10%' colspan = '1'><a href='pedidos_encerrados.php'>ver todos</a></td>
	</tr>
	<tr align='center'>
		<td>Nº</td>
		<td>Detalhe</td>
		<td>Data Compra</td>
		<td>Data Envio</td>
		<td>Rastreio</td>
		<td>Data Entrega</td>
	</tr>
	<?php
	$execbanco=mysqli_query($conexao,$sel);
	$verifica = "Encerrado";
	include('pedido_include.html');
	include('entregado.php');
	?>
</table>
		
<?php }

else{
        header("location:gerenciador.php");
}
?>