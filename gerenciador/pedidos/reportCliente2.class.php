<?php  
  require_once "../../conexao/conexao1.php";
  require_once "../../mpdf/mpdf.php";  
  
  
  
  class reportCliente extends mpdf{  

    // Atributos da classe  
    private $pdo  = null;  
    private $pdf  = null;
    private $css  = null;  
    private $titulo = null; 
 
    /*  
    * Construtor da classe  
    * @param $css  - Arquivo CSS  
    * @param $titulo - Título do relatório   
    */  
    public function __construct($css, $titulo) {  
      $this->pdo  = Conexao::getInstance();  
      $this->titulo = $titulo;
      $this->setarCSS($css);
    }
  
    /*  
    * Método para setar o conteúdo do arquivo CSS para o atributo css  
    * @param $file - Caminho para arquivo CSS  
    */  
    public function setarCSS($file){  
     if (file_exists($file)):  
       $this->css = file_get_contents($file);  
     else:  
       echo 'Arquivo inexistente!';  
     endif;  
    }  

    /*  
    * Método para montar o Cabeçalho do relatório em PDF  
    */  
    protected function getHeader(){  
       $data = date('j/m/Y');  
       $retorno = "<table class=\"tbl_header\" width=\"1000\">  
               <tr>  
                 <td align=\"left\">Biblioteca mPDF</td>  
                 <td align=\"right\">Gerado em: $data</td>  
               </tr>  
             </table>";  
       return $retorno;  
     }  

     /*  
     * Método para montar o Rodapé do relatório em PDF  
     */  
     protected function getFooter(){  
       $retorno = "<table class=\"tbl_footer\" width=\"1000\">  
               <tr>
                 <td align=\"right\">Página: {PAGENO}</td>  
               </tr>  
             </table>";  
       return $retorno;  
     } 
	 
    /*   
    * Método para construir a tabela em HTML com todos os dados  
    * Esse método também gera o conteúdo para o arquivo PDF  
    */
	
    private function getTabela(){  
      $color  = false;  
      $retorno = "";  
		
		$pedido=$_GET['pedido'];
	  
	  $sql1="select * from pedido inner join clientes on pedido.saram = clientes.saram
		inner join endereco on pedido.saram = endereco.saram
	  where pedido.id='$pedido'";  
      foreach ($this->pdo->query($sql1) as $reg):  
           $retorno .= "<td class='destaque'></td>";       
      endforeach;
	  
	  
      $retorno .= "<h2 style=\"text-align:center\">{$this->titulo}</h2>";  
      $retorno .= "
	  <table border='1' align='center' width='70%'>
		<tr align='center'><td colspan='2'>Remetente: </td></tr>
		<tr align='center'>
			<td>Endere&ccedil;o: </td>
			<td>CEP: </td>
		</tr>
		<tr align='center'>
			<td>Bairro: </td>
			<td>Cidade:  / </td>
		</tr>
	</table>
	<br><br>
	<table border='1' align='center' width='70%'>
		<tr align='center'><td colspan='2'>Destinat&aacute;rio: {$reg['nome']} {$reg['sobrenome']} </td></tr>
		<tr align='center'>
			<td colspan='2'>Endere&ccedil;o: {$reg['rua']}, {$reg['numero']}</td>
		</tr>
		<tr>
			<td>CEP: {$reg['cep']}</td>
			<td>Complemento: {$reg['complemento']}</td>
		</tr>
		<tr align='center'>
			<td>Bairro: {$reg['bairro']}</td>
			<td>Cidade: {$reg['cidade']} / {$reg['estado']}</td>
		</tr>
	</table>
	<br><br>
	  <table border='1' width='100%' align='center'>  
			<tr align='center'>
				<td>C&oacute;digo</td>
				<td>Produto</td>
				<td>Valor Unit&aacute;rio (R$)</td>
				<td>Quantidade</td>
				<td>Total</td>
			</tr>
			";
      return $retorno;  
    } 

    /*   
    * Método para construir o arquivo PDF  
    */  
    public function BuildPDF(){  
     $this->pdf = new mPDF('utf-8', 'A4-P');  
     $this->pdf->WriteHTML($this->css, 1);  
     $this->pdf->SetHTMLHeader($this->getHeader());  
     $this->pdf->SetHTMLFooter($this->getFooter());	 
     $this->pdf->WriteHTML($this->getTabela()); 
	 
    }   

    /*   
    * Método para exibir o arquivo PDF  
    * @param $name - Nome do arquivo se necessário grava-lo  
    */  
    public function Exibir($name = null) {  
     $this->pdf->Output($name, 'I');  
    }  
  }

?>  