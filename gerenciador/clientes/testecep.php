<?php
session_start();
if(isset($_SESSION['slogin']) && isset($_SESSION['ssenha'])){
?>

<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>
<html>
    <head>
		<title>ViaCEP Webservice</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<!-- Adicionando Javascript -->
		<script src='endereco.js' type="text/javascript"></script>
    </head>

    <body>
		<!-- Inicio do formulario -->
		<form method="post" action=".">
			<h2 align='center'>Dados</h2>
			<table align="center">
				<tr>
					<td>Nome: </td>
					<td><input type="text" name="nome"/></td>
				</tr>
				<tr>
					<td>Sobrenome: </td>
					<td><input type="text" name="sobrenome"/></td>
				</tr>
				<tr>
					<td>Saram: </td>
					<td><input type="text" name="Saram"/></td>
				</tr>
				<tr>
					<td>Telefone: </td>
					<td><input type="text" name="telefone"/></td>
				</tr>
				<tr>
					<td>Data de Nascimento: </td>
					<td><input type="text" name="nascimento"/></td>
				</tr>
				<tr>
					<td>Email: </td>
					<td><input type="text" name="email"/></td>
				</tr>
				<tr>
					<td>Login: </td>
					<td><input type="text" name="login"/></td>
				</tr>
				<tr>
					<td>Senha: </td>
					<td><input type="password" name="senha" /></td>
				</tr>
			</table>
			<h2 align='center'>Endereço</h2>
			<table align='center'>
				<tr>
					<td>Cep:</td>
					<td><input name="cep" type="text" id="cep" value="" size="10" maxlength="9"
					   onblur="pesquisacep(this.value);" /></td>
				</tr>
				<tr>
					<td>Rua:</td>
					<td><input name="rua" type="text" id="rua" size="60" /></td>
				</tr>
				<tr>
					<td>Numero:</td>
					<td><input name="numero" type="text" id="numero" size="4" /></td>
				</tr>
				<tr>
					<td>Bairro:</td>
					<td><input name="bairro" type="text" id="bairro" size="40" /></td>
				</tr>
				<tr>
					<td>Cidade:</td>
					<td><input name="cidade" type="text" id="cidade" size="40" /></td>
				</tr>
				<tr>
					<td>Estado:</td>
					<td><input name="uf" type="text" id="uf" size="2" /></td>
				</tr>
			</table>
		</form>
	</body>
</html>
<?php
	}
	else{
			header("location:gerenciador.php");
	}
?>