﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){

if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1) && ($_SESSION['gnivel'] != 3)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');

include("../conexao/conexao.php");
$codigo=$_POST['codigo'];
$sel="select*from clientes where codigo='$codigo'";
$execbanco=mysqli_query($conexao,$sel);
$dados=mysqli_fetch_array($execbanco);
?>

<html>
 <head>
    <title>Cadastro Clientes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Adicionando Javascript -->
    <script src='../script/endereco.js' type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/formularios.css">
</head>
<body>

<div class="box-form">
<h2 align="center"> Atualização Dados Pessoais:</h2>

	
	<input type='hidden' value="<?php echo $dados['codigo'];?>"/>
	<div class="form-group">
		<label class="label-input-style" for="saram">SARAM: ** </label>
		<input type="text" class="input-style" id="saram" name="saram" value="<?php echo $dados['saram'];?>" readonly />
	</div>

	<div class="form-group">
		<label class="label-input-style" for="nome">Nome: ** </label>
		<input type="text" class="input-style" id="nome" name="nome" value="<?php echo $dados['nome'];?>" readonly />
	</div>
	<div class="form-group">
		<label class="label-input-style" for="sobrenome">Sobrenome: ** </label>
		<input type="text" class="input-style" id="sobrenome" name="sobrenome" value="<?php echo $dados['sobrenome'];?>" readonly />
	</div>
	<div class="form-group">
		<label class="label-input-style" for="nascimento">Data de Nascimento: ** </label>
		<input type="text" class="input-style" id="nascimento" name="nascimento" value="<?php echo $dados['nascimento'];?>" readonly />
	</div>
	<div class="form-group">
		<label class="label-input-style" for="sexo">Sexo: ** </label>
		<input type="text" class="input-style" id="sexo" name="sexo" value="<?php echo $dados['sexo'];?>" readonly />
	</div>
	<form method="post" action="atualizacli.php">
		<div class="form-group">
			<label class="label-input-style" for="telefone">Telefone:  </label>
			<input type="text" class="input-style" id="telefone" name="telefone" value="<?php echo $dados['telefone'];?>"/>
		</div>	
		<div class="form-group">
			<label class="label-input-style" for="email">Email: </label>
			<input type="text" class="input-style" id="email" name="email" value="<?php echo $dados['email'];?>"/>
		</div>
		
		<div class="form-group">
				<input type="hidden" name="saram" value="<?php echo $dados['saram'];?>"/>
				<input type="reset" class="button-restaura" value="Restaurar"/>
				<input type="submit" class="button" value="Atualizar"/>
		</div>
	</form>
	<hr class="hr-aviso">
</div>
</body>
<?php
	}
	}
else{
        header("location:gerenciador.php");
}
?>