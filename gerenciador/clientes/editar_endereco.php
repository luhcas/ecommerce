<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1) && ($_SESSION['gnivel'] != 3)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>

<?php
include("../conexao/conexao.php");
$saram=$_POST['saram'];
?>
<html>
<head>
  <title>ViaCEP Webservice</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <!-- Adicionando Javascript -->
  <script src='../script/endereco.js' type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="../css/formularios.css">
</head>
<body>
  <div class="box-form">
    <h2 align='center'>Atualizar Endereço</h2>
    <form method='post' action='atualiza_endereco.php'>
    	<?php
    			include('../include/cad_endereco.html');
    	?>
    	<input type="hidden" name="saram" value="<?php echo $saram ?>"/>
    	<input type="reset" class="button-restaura" value="Limpar"/>
      <input type='submit' class="button" value='Atualizar' />
    </form>
  </div>
</body>
</html>
<?php
	}
}
else{
        header("location:gerenciador.php");
}
?>