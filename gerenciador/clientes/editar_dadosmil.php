﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1) && ($_SESSION['gnivel'] != 3)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');

include("../conexao/conexao.php");
$saram=$_POST['saram'];
$sel="select*from dados_militares where saram='$saram'";
$execbanco=mysqli_query($conexao,$sel);
$dados=mysqli_fetch_array($execbanco);

$sel1="select*from especialidade";
$execbanco1=mysqli_query($conexao,$sel1);
?>

<html>
 <head>
    <title>Cadastro Clientes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Adicionando Javascript -->
    <script src='../script/endereco.js' type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/formularios.css">
</head>
<body>

<div class="box-form">
	<h2 align="center"> Atualização Dados Militares</h2>
	<form method="post" action="atualiza_dadosmil.php">
		<div class="form-group">
			<label class="label-input-style" for="nome_guerra">Nome de Guerra</label>
			<input type='text' class="input-style" id="nome_guerra" name='nome_guerra' value="<?php echo $dados['nome_guerra'];?>"/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="grupamento">Grupamento</label>
				<?php echo $dados['grupamento'];?>
					<select class="input-style" id="grupamento" name="grupamento">
						<option value=""></option>
						<option value="Oficial General">Oficial General</option>
						<option value="Oficial Superior">Oficial Superior</option>
						<option value="Oficial Intermediário">Oficial Intermediário</option>
						<option value="Oficial Subalterno">Oficial Subalterno</option>
						<option value="Graduado">Graduado</option>
					</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="graduacao">Graduação</label>
				<?php echo $dados['graduacao'];?>
				<select class="input-style" id="graduacao" name="graduacao">
					<option value=""></option>
					<option value="Tenente Brigadeiro">Tenente Brigadeiro</option>
					<option value="Major Brigadeiro">Major Brigadeiro</option>
					<option value="Brigadeiro">Brigadeiro</option>
					<option value="Coronel">Coronel</option>
					<option value="Tenente coronel">Tenente coronel</option>
					<option value="Major">Major</option>
					<option value="Capitão">Capitão</option>
					<option value="1 Tenente">1º Tenente</option>
					<option value="2 Tenente">2º Tenente</option>
					<option value="Aspirante">Aspirante</option>
					<option value="Suboficial">Suboficial</option>
					<option value="Primeiro Sargento">Primeiro Sargento</option>
					<option value="Segundo Sargento">Segundo Sargento</option>
					<option value="Terceiro Sargento">Terceiro Sargento</option>
				</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="especialidade">Especialidade **</label>
				<input type='text' class="input-style" id="especialidade" name='especialidade' value="<?php echo $dados['especialidade'];?>" readonly />
		</div>
		<div class="form-group">
			<label class="label-input-style" for="unidade">Unidade</label>
				<input type='text' class="input-style" id="unidade" name='unidade' value="<?php echo $dados['unidade'];?>"/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="secao">Seção</label>
				<td><input type='text' class="input-style" id="secao" name='secao' value="<?php echo $dados['secao'];?>"/></td>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="ramal">Telefone</label>
			<td><input type='text' class="input-style" id="ramal" name='ramal' value="<?php echo $dados['ramal'];?>"/></td>
		</div>
		<div class="form-group">
			<input type="hidden" name="saram" value="<?php echo $dados['saram'];?>"/>	
			<input type="reset" class="button-restaura" value="Restaurar"/>
			<input type="submit" class="button" value="Atualizar"/>
		</div>
	</form>
	<hr class="hr-aviso">
</div>
</body>
</html>
<?php
	}
}
else{
        header("location:../gerenciador.php");
}
?>