﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
	if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1) && ($_SESSION['gnivel'] != 3)) {
	  // Destrói a sessão por segurança
	  session_destroy();
	  // Redireciona o visitante de volta pro login
	  header("Location: ../gerenciador.php"); exit;
	}
	else{
?>

<html>
	<head>
	    <title>Cadastro Clientes</title>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	    <!-- Adicionando Javascript -->
	    <script src='../script/endereco.js' type="text/javascript"></script>
	    <link rel="stylesheet" type="text/css" href="../css/formularios.css">
	</head>

<?php
$page_title = 'Home Page';
include('../include/headerg.html');
include("../conexao/conexao.php");
$saram=$_GET['saram'];

$sel="select*from clientes as c inner join endereco as e on c.saram = e.saram inner join dados_militares as d on c.saram = d.saram where c.saram='$saram'";
$execbanco=mysqli_query($conexao,$sel);

if(mysqli_num_rows($execbanco)==0){
        echo"<p align='center'><a href='../cadastros/form_cadastro_clientes.php'>Adcionar Novo</a></p>
			<p align='center'>Nenhum registro foi encontrado</p>";
}
else{
?>

<?php
	$cor1="silver";
	$cor2="gray";
	$cor=$cor1;
	while($dados=mysqli_fetch_array($execbanco)){
?>

<body>
	<div class="box-form">
		<form method="POST" action="editarcli.php">	
			<label class="label">Dados</label>
			<input type="hidden" name="codigo" value="<?php echo $dados['codigo'];?>"/>
			<input type="submit" class="button-editar" value="Editar"/>
		</form>
	</div>
<table width="80%" align='center'>
	<tr>
		<td class="c1">Saram</td>
		<td class="c2"><?php echo $dados['saram'];?></td>
	</tr>
	<tr>
		<td class="c1" width='50%'>Nome</td>
		<td class="c2" width='50%'><?php echo $dados['nome'];?></td>
	</tr>
	<tr>
		<td class="c1">Sobrenome</td>
		<td class="c2"><?php echo $dados['sobrenome'];?></td>
	</tr>
	<tr>
		<td class="c1">Telefone</td>
		<td class="c2"><?php echo $dados['telefone'];?></td>
	</tr>
	<tr>
		<td class="c1">Data de Nascimento</td>
		<td class="c2"><?php echo $dados['nascimento'];?></td>
	</tr>
	<tr>
		<td class="c1">Sexo</td>
		<td class="c2"><?php echo $dados['sexo'];?></td>
	</tr>
	<tr>
		<td class="c1">E-mail</td>
		<td class="c2"><?php echo $dados['email'];?></td>
	</tr>
</table>

<div class="box-form">
	<form method="post" action="editar_dadosmil.php">
		<label class="label">Dados Militares</label>
		<input type="hidden" name="saram" value="<?php echo $dados['saram'];?>"/>
		<input type="submit" class="button-editar" value="Editar"/>
	</form>
</div>
<table width="80%" align='center'>
	<tr>
		<td class="c1">Nome de Guerra</td>
		<td class="c2"><?php echo $dados['nome_guerra'];?></td>
	</tr>
	<tr>
		<td class="c1" width='50%'>Grupamento</td>
		<td class="c2" width='50%'><?php echo $dados['grupamento'];?></td>
	</tr>
	<tr>
		<td class="c1" width='50%'>Graduação / Patente</td>
		<td class="c2" width='50%'><?php echo $dados['graduacao'];?></td>
	</tr>
	<tr>
		<td class="c1">Especialidade</td>
		<td class="c2"><?php echo $dados['especialidade'];?></td>
	</tr>
	<tr>
		<td class="c1">Unidade</td>
		<td class="c2"><?php echo $dados['unidade'];?></td>
	</tr>
	<tr>
		<td class="c1">Seção</td>
		<td class="c2"><?php echo $dados['secao'];?></td>
	</tr>
	<tr>
		<td class="c1">Telefone</td>
		<td class="c2"><?php echo $dados['ramal'];?></td>
	</tr>
</table>

<div class="box-form">
	<form method="post" action="editar_endereco.php">
		<label class="label">Endereço</label>
		<input type="hidden" name="saram" value="<?php echo $dados['saram'];?>"/>
		<input type="submit" class="button-editar" value="Editar"/>
	</form>
</div>

<table width="80%" align='center'>
	<tr>
		<td class="c1" width='50%'>CEP</td>
		<td class="c2" width='50%'><?php echo $dados['cep'];?></td>
	</tr>
	<tr>
		<td class="c1">Rua</td>
		<td class="c2"><?php echo $dados['rua'];?></td>
	</tr>
	<tr>
		<td class="c1">Numero</td>
		<td class="c2"><?php echo $dados['numero'];?></td>
	</tr>
	<tr>
		<td class="c1">Complemento</td>
		<td class="c2"><?php echo $dados['complemento'];?></td>
	</tr>
	<tr>
		<td class="c1">Bairro</td>
		<td class="c2"><?php echo $dados['bairro'];?></td>
	</tr>
	<tr>
		<td class="c1">Cidade</td>
		<td class="c2"><?php echo $dados['cidade'];?></td>
	</tr>
	<tr>
		<td class="c1">Estado</td>
		<td class="c2"><?php echo $dados['estado'];?></td>
	</tr>
</table>
<?php if ($_SESSION['gnivel'] == 1){ ?>
<table width="30%" align='center'>
	<tr align='center'>
		<td><h2 align='center'>Pedidos Realizados</h2></td>
	</tr>
</table>

<?php
}
$sel="select * from pedido where saram='$saram'";
$execbanco=mysqli_query($conexao,$sel);

if(mysqli_num_rows($execbanco)==0){
	echo"<p align='center'>Nenhum Pedido Realizado</p>";
}
else{
?>
	<table class='table-pedido' width='80%' align='center'>
		<tr class="c3">			
			<td class="c4">Pedido: </td>
			<td class="c4">Data do Pedido</td>
			<td class="c4">Qtde de Itens</td>
			<td class="c4">Total do Pedido</td>
			<td class="c4">Status</td>
			<td class="c4">Detalhes</td>
		</tr>
<?php
	while($dados=mysqli_fetch_array($execbanco)){
		$pedido = $dados['id'];
?>
		<tr class="c3">
			<td class="c4"><?php echo $pedido; ?></td>
			<td class="c4"><?php echo $dados['data_compra'];?></td>
			<?php
				$soma_quantidade = 0;
				$valor_total = $dados['valor'] + $dados['frete'];
				$status = $dados['status'];
				$sel1="select quantidade from itens_pedido where id_pedido='$pedido'";
				$execbanco1=mysqli_query($conexao,$sel1);
				while($dados=mysqli_fetch_array($execbanco1)){
					 $quantidade = $dados['quantidade'];
					 $soma_quantidade += $quantidade;
				}?>
			<td class="c4"><?php echo $soma_quantidade ?></td>
			<td class="c4">R$ <?php echo number_format($valor_total, 2,',','.') ?></td>
			<td class="c4"><?php echo $status ?></td>
			<td class="c4">
				<form method='POST' action='pedido_cliente_detalhado.php'>
					<input name='detalhes' type='hidden' <?php echo"value=' $pedido'";?>/>
					<input type='submit' class='button-detalhes' value='Ver mais detalhes'/>
				</form>
			</td>
		</tr>
<?php
}
?>
	</table>
<?php
}
?>
<table width="30%" align='center'>
	<tr align='center'>
		<td><h2 align='center'>Alterações Realizadas</h2></td>
	</tr>
</table>
<p align='center'>Em construção</p>
<table width="80%" align='center' border='1'>
	<tr align='center'>
		<td>Campo Alterado</td>
		<td>Valor Anterior</td>
		<td>Novo Valor</td>
		<td>Alterado Por</td>
		<td>Data da Alteração</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
<p align='center'>Em construção</p>
<?php
}
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] == 1)) {
	echo"<p align='center'><a href='lista_de_clientes.php' style='text-decoration: none; color: #FF0000; font-size: 20px;'>VOLTAR</a></p>";
} else if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] == 3)){
	echo"<p align='center'><a href='../paginas/pessoal_militar.php' style='text-decoration: none; color: #FF0000; font-size: 20px;'>VOLTAR</a></p>";
}
?>
<?php
	if($cor==$cor1){$cor=$cor2;}
	else{$cor=$cor1;}
}
?>
</table>
</body>
<?php
	}
}

else{
        header("location:../gerenciador.php");
}
?>