﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>

<?php
include("../conexao/conexao.php");
$codigo=$_POST['codigo'];

$sel="select*from produto where codigo='$codigo'";
$execbanco=mysqli_query($conexao,$sel);
$dados1=mysqli_fetch_array($execbanco);

$sel1="select*from especialidade";
$execbanco1=mysqli_query($conexao,$sel1);

$sel2="select*from tamanho";
$execbanco2=mysqli_query($conexao,$sel2);

$sel3="select*from categoria";
$execbanco3=mysqli_query($conexao,$sel3);
?>
<html>
 <head>
    <title>Cadastro Clientes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Adicionando Javascript -->
    <script src='../script/endereco.js' type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/formularios.css">
</head>
<body>
<div class="box-form">
	<h2 align="center"> Atualização do Produto: <?php echo $dados1['nome'];?></h2>
	<form method="post" action="atualizapro.php" enctype="multipart/form-data">
	    <div class="form-group">
			<label class="label-input-style" for="nome">Nome: </label>
	        <input type="text" class="input-style" id="nome" name="nome" value="<?php echo $dados1['nome'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="tamanho">Tamanho: </label>
			<?php echo $dados1['tamanho']; ?>
			<select class="input-style" id="tamanho" name="tamanho">
				<option value=""></option>
				<?php
					while($dados=mysqli_fetch_array($execbanco2)){
						echo"<option value='".$dados['tamanho']."'>".$dados['tamanho']."</option>";
					}
				?>
			</select>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="categoria">Categoria: </label>
	        <?php echo $dados1['categoria']; ?>
			<select class="input-style" id="categoria" name="categoria">
				<option value=""></option>
				<?php
					while($dados=mysqli_fetch_array($execbanco3)){
						echo"<option value='".$dados['categoria']."'>".$dados['categoria']."</option>";
					}
				?>
			</select>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="grupamento">Grupamento </label>
			<?php echo $dados1['grupamento'];?>
			<select class="input-style" id="grupamento" name="grupamento">
				<option value=""></option>
				<option value="TODOS">TODOS</option>
				<option value="Oficial General">Oficial General</option>
				<option value="Oficial Superior">Oficial Superior</option>
				<option value="Oficial Intermediário">Oficial Intermediário</option>
				<option value="Oficial Subalterno">Oficial Subalterno</option>
				<option value="Graduado">Graduado</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="graduacao">Graduação </label>
			<?php echo $dados1['graduacao']; ?>
			<select class="input-style" id="graduacao" name="graduacao">
				<option value=""></option>
				<option value="TODOS">TODOS</option>
				<option value="Tenente Brigadeiro">Tenente Brigadeiro</option>
				<option value="Major Brigadeiro">Major Brigadeiro</option>
				<option value="Brigadeiro">Brigadeiro</option>
				<option value="Coronel">Coronel</option>
				<option value="Tenente coronel">Tenente coronel</option>
				<option value="Major">Major</option>
				<option value="Capitão">Capitão</option>
				<option value="1 Tenente">1º Tenente</option>
				<option value="2 Tenente">2º Tenente</option>
				<option value="Aspirante">Aspirante</option>
				<option value="Suboficial">Suboficial</option>
				<option value="Primeiro Sargento">Primeiro Sargento</option>
				<option value="Segundo Sargento">Segundo Sargento</option>
				<option value="Terceiro Sargento">Terceiro Sargento</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="especialidade">Especialidade: </label>
			<?php echo $dados1['especialidade']; ?>
			<select class="input-style" id="especialidade" name="especialidade">
				<option value=""></option>
				<option value="TODOS">TODOS</option>
				<?php
					while($dados=mysqli_fetch_array($execbanco1)){
						echo"<option value='".$dados['especialidade']."'>".$dados['especialidade']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="sexo">Sexo </label>
			<?php echo $dados1['sexo']; ?>
			<select class="input-style" id="sexo" name="sexo">
				<option value=""></option>
				<option value="AMBOS">AMBOS</option>
				<option value="MASCULINO">MASCULINO</option>
				<option value="FEMININO">FEMININO</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="descricao">Descrição:  </label>
	        <input class="input-style" id="descricao" type="text" name="descricao" value="<?php echo $dados1['descricao'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="comprimento">Comprimento: </label>
	        <input class="input-style" id="comprimento" type="text" name="comprimento" value="<?php echo $dados1['comprimento'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="altura"> Altura: </label>
	        <input class="input-style" id="altura" type="text" name="altura" value="<?php echo $dados1['altura'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="largura">Largura: </label>
	        <input class="input-style" id="largura" type="text" name="largura" value="<?php echo $dados1['largura'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="peso">Peso: </label>
	        <input class="input-style" id="peso" type="text" name="peso" value="<?php echo $dados1['peso'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="quantidade">Quantidade: </label>
	        <input class="input-style" id="quantidade" type="text" name="quantidade" value="<?php echo $dados1['quantidade'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="preco">Preço: </label>
	        <input type="text" class="input-style" id="preco" name="preco" value="<?php echo $dados1['preco'];?>"/>
	    </div>
	    <div class="form-group">
			<label class="label-input-style" for="foto">Foto: </label>
	        <input type="file" class="input-style" id="foto" name="foto" value="<?php echo $dados1['foto'];?>"/>
	    </div>
	    <div class="form-group">
	        <input type="hidden" name="codigo" value="<?php echo $dados1['codigo'];?>"/>
	        <input type="reset" class="button-restaura" value="Restaurar"/>  
	        <input type="submit" class="button" value="Atualizar"/>
	    </div>
	</form>
</div>
</body>
</html>
<?php
}
}
else{
        header("location:gerenciador.php");
}
?>