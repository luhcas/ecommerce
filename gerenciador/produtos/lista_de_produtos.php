﻿<style type="text/css">
        .c1{border:solid 1px black; background-color:#99ccff;
                font-weight:bold;text-align: center;}
        .c2{border:solid 1px black;}
</style>

<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>

<?php
include("../conexao/conexao.php");
$sel="select*from produto order by codigo asc";
$execbanco=mysqli_query($conexao,$sel);
if(mysqli_num_rows($execbanco)==0){
        echo"<p align='center'><a href='form_cadastro_produtos.php'>Adcionar Novo</a></p>
			<p align='center'>Nenhum registro foi encontrado</p>";
}
else{
?>

<h2 align="center">Lista de produtos</h2>
<p align="center">
        <a href="../cadastros/form_cadastro_produtos.php">Adcionar Novo</a>
</p>

<table width="100%">
        <tr>
                <td class="c1">Código</td>
                <td class="c1">Nome</td>
				<td class="c1">Tamanho</td>
                <td class="c1">Categoria</td>
				<td class="c1">Grupamento</td>
				<td class="c1">Graduação</td>
				<td class="c1">Especialidade</td>
				<td class="c1">Sexo</td>
                <td class="c1">Descrição</td>
				<td class="c1">Comprimento</td>
				<td class="c1">Altura</td>
				<td class="c1">Largura</td>
				<td class="c1">Peso (G)</td>
				<td class="c1">Quantidade</td>
                <td class="c1">Preço</td>
                <td class="c1">Foto</td>
                <td class="c1">Editar</td>
        </tr>
        <?php
        $cor1="silver";
        $cor2="gray";
        $cor=$cor1;
        while($dados=mysqli_fetch_array($execbanco)){
        ?>
        <tr bgcolor="<?php echo $cor;?>" align='center'>
                <td class="c2"><?php echo $dados['codigo'];?></td>
                <td class="c2"><?php echo $dados['nome'];?></td>
				<td class="c2"><?php echo $dados['tamanho'];?></td>
                <td class="c2"><?php echo $dados['categoria'];?></td>
				<td class="c2"><?php echo $dados['grupamento'];?></td>
				<td class="c2"><?php echo $dados['graduacao'];?></td>
				<td class="c2"><?php echo $dados['especialidade'];?></td>
				<td class="c2"><?php echo $dados['sexo'];?></td>
                <td class="c2"><?php echo $dados['descricao'];?></td>
				<td class="c2"><?php echo $dados['comprimento'];?></td>
				<td class="c2"><?php echo $dados['altura'];?></td>
				<td class="c2"><?php echo $dados['largura'];?></td>
				<td class="c2"><?php echo $dados['peso'];?></td>
				<td class="c2"><?php echo $dados['quantidade'];?></td>
                <td class="c2"><?php echo "R$ ".number_format($dados['preco'], 2, ',', '.');?></td>
                <td class="c2"><img src="../../imagens/<?php echo $dados['foto'];?>" width="100px" height="100px" align="center"/></td>
                <form method="post" action="editarpro.php">
                        <td class="c2" width="10">
                                <input type="hidden" name="codigo"
                                       value="<?php echo $dados['codigo'];?>"/>
                                <input type="submit" value="Editar"/>
                        </td>
                </form>
        </tr>
        <?php
                if($cor==$cor1){$cor=$cor2;}
                else{$cor=$cor1;}
        }
        ?>
</table>
<?php
	}
}
}
else{
        header("location:gerenciador.php");
}
?>