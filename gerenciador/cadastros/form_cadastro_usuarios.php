﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');
?>

<h2 align="center"> Cadastro de Usuarios</h2>
<form method="post" action="cadusu.php">
        <table align="center">
			<tr>
				<td>Saram: </td>
				<td><input type="text" name="saram"/></td>
			</tr>
			<tr>
				<td>Nome: </td>
				<td><input type="text" name="nome"/></td>
			</tr>
			<tr>
				<td>Sobrenome: </td>
				<td><input type="text" name="sobrenome"/></td>
			</tr>
			<tr>
				<td>Graduação</td>
				<td><select name="graduacao">
						<option value=""></option>
						<option value="Tenente Brigadeiro">Tenente Brigadeiro</option>
						<option value="Major Brigadeiro">Major Brigadeiro</option>
						<option value="Brigadeiro">Brigadeiro</option>
						<option value="Coronel">Coronel</option>
						<option value="Tenente coronel">Tenente coronel</option>
						<option value="Major">Major</option>
						<option value="Capitão">Capitão</option>
						<option value="1 Tenente">1º Tenente</option>
						<option value="2 Tenente">2º Tenente</option>
						<option value="Aspirante">Aspirante</option>
						<option value="Suboficial">Suboficial</option>
						<option value="Primeiro Sargento">Primeiro Sargento</option>
						<option value="Segundo Sargento">Segundo Sargento</option>
						<option value="Terceiro Sargento">Terceiro Sargento</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Nome de Guerra: </td>
				<td><input type="text" name="nome_guerra"/></td>
			</tr>
			<tr>
				<td>Unidade: </td>
				<td><input type="text" name="unidade"/></td>
			</tr>
			<tr>
				<td>Seção: </td>
				<td><input type="text" name="secao"/></td>
			</tr>
			<tr>
				<td>Telefone: </td>
				<td><input type="text" name="telefone"/></td>
			</tr>
			<tr>
				<td>Email: </td>
				<td><input type="text" name="email"/></td>
			</tr>
			<tr>
				<td>Login: </td>
				<td><input type="text" name="login"/></td>
			</tr>
			<tr>
				<td>Senha: </td>
				<td><input type="password" name="senha" /></td>
			</tr>
			<tr>
				<td>Tipo:</td>
				<td><select name="tipo">
					<option value=""> --------------------- </option>
					<option value="Administrador"> Administrador </option>
					<option value="Vendedor"> Vendedor </option>
					<option value="Operador"> Operador </option>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right">
					<input type="submit" value="Enviar"/>
					<input type="reset" value="Limpar"/>
				</td>
			</tr>
        </table>
</form>
<?php
}
}
else{
        header("location:gerenciador.php");
}
?>