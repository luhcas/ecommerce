﻿<?php
session_start();
if(isset($_SESSION['glogin']) && isset($_SESSION['gsenha'])){
if (!isset($_SESSION['gnivel']) or ($_SESSION['gnivel'] != 1) && ($_SESSION['gnivel'] != 3)) {
  // Destrói a sessão por segurança
  session_destroy();
  // Redireciona o visitante de volta pro login
  header("Location: ../gerenciador.php"); exit;
}
else{
?>  
 
<?php
$page_title = 'Home Page';
include('../include/headerg.html');

include("../conexao/conexao.php");
$sel="select*from especialidade";
$execbanco=mysqli_query($conexao,$sel);
?>
<html>
 <head>
    <title>Cadastro Clientes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Adicionando Javascript -->
    <script src='../script/endereco.js' type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../css/formularios.css">
</head>
<body>
<div class="box-form">
	<h2 align="center"> Cadastro de Clientes</h2>
	<form method="post" action="cadcli.php">
		<hr class="hr-user">
		<div class="form-group">
			<label class="label-input-style" for="saram">Saram: </label>
			<input type="text" class="input-style" id="saram" name="saram" placeholder="Digite o Saram" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="nome">Nome: </label>
			<input type="text" class="input-style" id="nome" name="nome" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="sobrenome">Sobrenome: </label>
			<input type="text" class="input-style" id="sobrenome" name="sobrenome" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="telefone">Telefone: </label>
			<input type="text" class="input-style" id="telefone" name="telefone" placeholder="(xx) xxxx-xxxx" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="nascimento">Data de Nascimento: </label>
			<input type="date" class="input-style" id="nascimento" name="nascimento" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="email">Email: </label>
			<input type="text" class="input-style" id="email" name="email" placeholder="exemplo@email.com" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="nome_guerra">Nome de Guerra: </label>
			<input type="text" class="input-style" id="nome_guerra" name="nome_guerra" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="grupamento">Grupamento</label>
			<select class="input-style" name="grupamento" required>
				<option value=""></option>
				<option value="Oficial General">Oficial General</option>
				<option value="Oficial Superior">Oficial Superior</option>
				<option value="Oficial Intermediário">Oficial Intermediário</option>
				<option value="Oficial Subalterno">Oficial Subalterno</option>
				<option value="Graduado">Graduado</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="graduacao">Graduação</label>
			<select class="input-style" name="graduacao" required>
				<option value=""></option>
				<option value="Tenente Brigadeiro">Tenente Brigadeiro</option>
				<option value="Major Brigadeiro">Major Brigadeiro</option>
				<option value="Brigadeiro">Brigadeiro</option>
				<option value="Coronel">Coronel</option>
				<option value="Tenente coronel">Tenente coronel</option>
				<option value="Major">Major</option>
				<option value="Capitão">Capitão</option>
				<option value="1 Tenente">1º Tenente</option>
				<option value="2 Tenente">2º Tenente</option>
				<option value="Aspirante">Aspirante</option>
				<option value="Suboficial">Suboficial</option>
				<option value="Primeiro Sargento">Primeiro Sargento</option>
				<option value="Segundo Sargento">Segundo Sargento</option>
				<option value="Terceiro Sargento">Terceiro Sargento</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="especialidade">Especialidade: </label>
			<select class="input-style" name="especialidade" required>
				<option value=""></option>
				<?php
					while($dados=mysqli_fetch_array($execbanco)){
						echo"<option value='".$dados['especialidade']."'>".$dados['especialidade']."</option>";
					}
				?>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="sexo">Sexo</label>
			<select class="input-style" name="sexo" required>
				<option value=""></option>
				<option value="MASCULINO">MASCULINO</option>
				<option value="FEMININO">FEMININO</option>
			</select>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="unidade">Unidade: </label>
			<input type="text" class="input-style" id="unidade" name="unidade" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="secao">Seção: </label>
			<input type="text" class="input-style" id="secao" name="secao" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="ramal">Telefone: </label>
			<input type="text" class="input-style" id="ramal" name="ramal" placeholder="(xx) xxxx-xxxx" required/>
		</div>
		<div class="form-group">
			<label class="label-input-style" for="senha">Senha: </label>
			<input type="password" class="input-style" id="senha" name="senha" required/>
		</div>
		<h2 align='center'>Endereço</h2>
		<?php
			include('../include/cad_endereco.html');
		?>
		<div class="form-group">
			<input type="reset" class="button-restaura" value="Limpar"/>
			<input class="button" type="submit" value="Enviar"/>
		</div>
	</form>
</div>
</body>
</html>
<?php
	}
	}
	else{
			header("location:gerenciador.php");
	}
?>